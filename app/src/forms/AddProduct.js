import React from 'react'
import {Form, Row, Col, Button} from 'react-bootstrap';

export default class AddProduct extends React.Component{

    // constructor(props) {
    //     super(props);

    //     // this.state = {
    //     //     catalogo: '',
    //     //     area: '',
    //     //     item: ''
    //     // }
    
    //     // this.handleChange = this.handleChange.bind(this);
    //     // this.handleSubmit = this.handleSubmit.bind(this);
    // }

    // handleChange(event) {
    //     event.preventDefault();

    //     this.setState({[event.target.name]: event.target.value});
        
    // }

    // handleSubmit(event){
    //     const listProducts = this.props.listProducts;
    //     const listProductAdd = this.state;

    //     listProducts.push(listProductAdd);
    //     localStorage.setItem('data', JSON.stringify(listProducts));

    //     this.setState({catalogo: '', area: '', item: '', });

    //     window.reload();
    //     //window.scrollTo(0,document.body.scrollHeight);
    //     event.preventDefault();
    // }
    

    render() {

		
		return (
            <Form onSubmit={this.props.handleSubmit}>
                <Row>
                    <Col>
                        <Form.Group controlId="formBasicEmail">
                            <Form.Label>Catálogo</Form.Label>
                            <Form.Control type="text"  placeholder="Catálogo" name="catalogo" id="catalogo" required/>
                        </Form.Group>
                    </Col>
                    <Col>
                        <Form.Group controlId="formBasicEmail">
                            <Form.Label>Área</Form.Label>
                            <Form.Control type="text" placeholder="Área" name="area" id="area" required/>
                        </Form.Group>
                    </Col>
                    <Col>
                        <Form.Group controlId="formBasicEmail">
                            <Form.Label>Item</Form.Label>
                            <Form.Control type="text" placeholder="Item" name="item" id="item" required/>
                        </Form.Group>
                    </Col>
                    <Col>
                        <Button variant="secondary" type="submit" className="mt--30" block>
                            Aregar nuevo producto
                        </Button>
                    </Col>
                </Row>
            </Form>
		);
	}
}

