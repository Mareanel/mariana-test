import React from 'react';
import {Table, ButtonToolbar, Button} from 'react-bootstrap';



export default class ProductTable extends React.Component{

	render() {
		const listProducts = this.props.listProducts;
		
		return (
			<div>
				<Table  bordered responsive >
					<thead>
						<tr>
							<th>Catálogo</th>
							<th>Área</th>
							<th>Item</th>
							<th></th>
						</tr>
					</thead>


					<tbody>
						{listProducts.map((listProducts, index) => (
							<tr key={index} id={index}>
								<td>{listProducts.catalogo}</td>
								<td>{listProducts.area}</td>
								<td>{listProducts.item}</td>
								<td>
									<ButtonToolbar>
										<Button variant="secondary" className="mr--10">Editar</Button>

										<Button  onClick={() => this.props.deleteProduct(index)}  variant="danger">Eliminar</Button>
									</ButtonToolbar>
								</td>
							</tr>
						))}
					</tbody>
				</Table>
			</div>
		);
	}
}

