import React from 'react';
import axios from 'axios';
import {Container, Card, Button, Alert, Modal} from 'react-bootstrap';

import '../node_modules/bootstrap/dist/css/bootstrap.css';
import './stylesheets/main.css';


import ProductTable from './tables/ProductTable';
import AddProduct from './forms/AddProduct';


  
export default class App extends React.Component{
	
	constructor(props) {
        super(props);
		
		this.state = {
			listProducts: [],
			addProduct:  false,
			addAlert: false,
		}
		this.nuevo = {
			// addProducts:[
			// 	{
					catalogo: '',
					area: '',
					item: ''
			// 	}
			// ]
			
		}

		this.showFormAddProduct = this.showFormAddProduct.bind(this);
		this.handleChange = this.handleChange.bind(this);
		this.handleSubmit = this.handleSubmit.bind(this);
		this.deleteProduct = this.deleteProduct.bind(this);

	}

	componentDidMount() {
		axios.get('../catalogoServicios.json')
			.then(res => {
			const listProducts = res.data;
			this.setState({ listProducts });

		})
	}

	showFormAddProduct(){
		this.setState({ addProduct: true});
	}


	handleChange(event) {
        event.preventDefault();

        this.setState({[event.target.name]: event.target.value});
        
    }

    handleSubmit(event){
        const listProducts = this.state.listProducts;
		//const listProductAdd = this.state;
		
		let catalogo = document.getElementById("catalogo"),
		    area = document.getElementById("area"),
		    item = document.getElementById("item");
			 
		let user = {
			catalogo: catalogo.value,
			area: area.value,
			item: item.value
		};

		listProducts.push(user);
        localStorage.setItem('data', JSON.stringify(listProducts));

        
        window.reload();
        //window.scrollTo(0,document.body.scrollHeight);
        event.preventDefault();
    }
    

	deleteProduct(idProduct){
		let elementProduct = document.getElementById(idProduct);
		elementProduct.parentNode.removeChild(elementProduct);
		//elementProduct.removeChild(elementProduct.childNodes[elementProduct]);

		localStorage.removeItem('data', idProduct);
	}

	render() {

		let properties = this.state.listProducts;

		if ('data' in localStorage){
			properties = JSON.parse(localStorage.getItem('data'));
		}
		
		return (
			<div className="test-wrapper">
				<header className="test-header">
					<Container>
						<h1>Challenge Front End</h1>
					</Container>
				</header>

				<Container>
					<div>

						{this.state.addProduct ? 
							<Card className="section-add-product">
								<Card.Body>
									<h2>Agregar producto</h2>
									
									<AddProduct showAlert={this.showAlert} handleSubmit={this.handleSubmit} handleChange={this.handleChange}
												catalogo={this.state.catalogo} area={this.state.area} item={this.state.item}/>
								

								</Card.Body>
							</Card>
						: 
							<div className="text-right">
								<Button onClick={this.showFormAddProduct} variant="secondary">Agregar producto</Button>
							</div>
						}
					</div>
					
					
					<Card className="mt--30">
						<Card.Body>
							<h2>Listado de productos</h2>
							<ProductTable listProducts={properties} deleteProduct={this.deleteProduct}/>
						</Card.Body>
					</Card>

				</Container>
			</div>
		);
	}
}

